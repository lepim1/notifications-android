package com.example.leonardopimentelferreira.notification;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by leonardopimentelferreira on 2/23/15.
 */
public class ILeapesService extends IntentService{
    public static final String PREFS_ILEAPS = "PREFS_ILEAPS";
    public static final String SHARED_PREFS_L_UPDATE = "L_UPDATE";

    public ILeapesService() {
        super("ILeapesService");
        Log.e("ILeapesService: ", "Constructor");
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {
        // Gets data from the incoming Intent
        String dataString = workIntent.getDataString();
        // Do work here, based on the contents of dataString
        if(dataString !=null)
            Log.e("ILeapsService", dataString);


        //In order to make a HTTP connection, it is necessary the creation of Thread
        new Thread(new Runnable() {
            public void run() {
                SharedPreferences prefs = getSharedPreferences(PREFS_ILEAPS, MODE_PRIVATE);
                String lUpdate = prefs.getString(SHARED_PREFS_L_UPDATE, null);
                System.out.println("Shared Preferences Last Update: "+lUpdate);
                if(lUpdate==null)
                    lUpdate="";
                try {
                    String query = URLEncoder.encode(lUpdate, "utf-8");
                    final JSONObject jNewNotifications = downloadURLtoJSON("http://10.6.224.116/ileaps/GetIncidents.php?l_update=" + query);
                    createSimpleNotification(jNewNotifications);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    /*
    // Reads an InputStream and converts it to a String.
    public String readHttpResponse(InputStream stream) throws IOException, UnsupportedEncodingException {
        // json is UTF-8 by default
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
        StringBuilder sb = new StringBuilder();

        String line = null;
        while ((line = reader.readLine()) != null)
        {
            sb.append(line + "\n");
        }
        return sb.toString();
    }*/
    public String readHttpResponse(InputStream stream) throws IOException, UnsupportedEncodingException {
        BufferedReader r = new BufferedReader(new InputStreamReader(stream));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    private JSONObject downloadURLtoJSON(String URL)
    {
        InputStream in;
        JSONObject jResponse = null;
        try {
            in = openHttpConnection(URL);
            String strhttpResponse = readHttpResponse(in);
            Log.e("HTTP response: ", strhttpResponse);
            jResponse = new JSONObject(strhttpResponse);
            //Log.e("New notifications: ", String.valueOf(new_notifications));
            in.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jResponse;
    }

    private InputStream openHttpConnection(String urlString)
            throws IOException
    {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }

        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setReadTimeout(10000 /* milliseconds */);
            httpConn.setConnectTimeout(15000 /* milliseconds */);
            httpConn.setDoInput(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
            Log.e("Response: ", String.valueOf(response));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new IOException("Error connecting");
        }
        return in;
    }

    public void createSimpleNotification(JSONObject jNewNotifications){
        String lUpdate = null;
        try {
            lUpdate = jNewNotifications.getString("l_update");
            JSONArray notifications = jNewNotifications.getJSONArray("incidents");
            for(int i=0; i<notifications.length();i++){
                JSONObject json_data = notifications.getJSONObject(i);
                Log.i("log_tag", "_id " + json_data.getInt("inc_id") +
                                ", incident type " + json_data.getString("inc_type") +
                                ", datetime " + json_data.getString("date_time") +
                                ", l_update " + json_data.getString("l_update"));

                buildNewIncidentNotification(json_data);
            }

            SharedPreferences prefs = getSharedPreferences(PREFS_ILEAPS, MODE_PRIVATE) ;
            SharedPreferences.Editor editor = prefs.edit ();
            editor.putString(SHARED_PREFS_L_UPDATE, lUpdate) ;
            editor.commit() ;
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void buildNewIncidentNotification(JSONObject json_data) {
        if(json_data != null) {
            try {
                NotificationCompat.Builder mBuilder =
                    null;

                mBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("ILeaps: "+json_data.getString("inc_type"))
                        .setContentText("New incident at "+ formatDate(json_data.getString("l_update")));

                Intent notifyIntent =
                        new Intent(this, MainActivity.class);

                PendingIntent resultPendingIntent
                        = PendingIntent.getActivity(getBaseContext(),
                        0, notifyIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                int notifyID = json_data.getInt("inc_id");
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                // mId allows you to update the notification later on.
                mNotificationManager.notify(notifyID, mBuilder.build());
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e){
                e.printStackTrace();
            }
        } else {
            Log.e("ILeapesService buildNotifications", "No new notifications");
        }
    }

    private String formatDate(String dateString) {
        try {
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss" /* 10-Sep-2013 09:53:37*/);
            Date d = sd.parse(dateString);
            sd = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
            return sd.format(d);
        } catch (ParseException e) {
        }
        return "";
    }
}
