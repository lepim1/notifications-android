package com.example.leonardopimentelferreira.notification;

/**
 * Created by leonardopimentelferreira on 6/5/15.
 *
 * Defines several constants used by this app.
 */
public interface Constants {
    // Alarm id, this identifies the an alarm so that when you want to cancel or update it you can find it
    public static final int ALARM_ID = 2015;
}

