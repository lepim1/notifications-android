package com.example.leonardopimentelferreira.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by leonardopimentelferreira on 6/4/15.
 *
 * This is a Broadcast Receiver https://developer.android.com/reference/android/content/BroadcastReceiver.html
 * It starts a background service to execute a task such as getting new notifications from the server.
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent alarmServiceIntent = new Intent(context, ILeapesService.class);
        context.startService(alarmServiceIntent);
        Log.e("AlarmReceiver", "Called context.startService from AlarmReceiver.onReceive");
    }
}