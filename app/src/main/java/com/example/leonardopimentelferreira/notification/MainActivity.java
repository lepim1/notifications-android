package com.example.leonardopimentelferreira.notification;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        createSimpleNotification();
       // createService();
//        createComplexNotification();
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ILeapesService Running", String.valueOf(isMyServiceRunning(ILeapesService.class)));
            }
        });

        //----------------------------------SET and CANCEL an Alarm--------------------------------------
        Intent alarmReceiverIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, Constants.ALARM_ID, alarmReceiverIntent, 0);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        //alarmManager.cancel(pendingIntent);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 15000L, 15000L, pendingIntent);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Do not work prior Android 4.1
     */
    public void createComplexNotification(){
        //Builder must include a small icon "setSmallIcon()", a title "setContentTitle()" and a detail text, "setContentText()"
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha)
                        .setContentTitle("New incident")
                        .setContentText("You received 6 messages");

        int displayMaxNumOfNotifications = 4;

        //-----------------Creates several lines at the notification---------------
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String[] events = new String[6];
        // Sets a title for the Inbox in expanded layout
        //inboxStyle.setBigContentTitle("Event tracker details:");
        inboxStyle.setSummaryText("Number of New Incidents:");
        // Moves events into the expanded layout
        for (int i=0; i < displayMaxNumOfNotifications; i++) {
            events[i]= "Today at 4:1"+i+"PM - Someone needs help!";
            inboxStyle.addLine(events[i]);
        }
        mBuilder.setNumber(events.length);
        inboxStyle.addLine("...");
        // Moves the expanded layout object into the notification object.
        mBuilder.setStyle(inboxStyle);
        //-----------------end creates several lines at the notification---------------

        //this is optional, it removes the notification when the user clicks on it
        //mBuilder.setAutoCancel(true);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, ResultActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(ResultActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);

        // Define the Notification's Action which in this case starts an Activity in your application.
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

        //Add action buttons to the notification
        mBuilder.addAction(R.drawable.abc_ic_go_search_api_mtrl_alpha, "Accept", resultPendingIntent);
        mBuilder.addAction(R.drawable.abc_ic_go_search_api_mtrl_alpha, "Decline", resultPendingIntent);

        //associates the PendingIntent with a gesture. Starts an activity when the user clicks the notification, add the PendingIntent by calling setContentIntent()
        mBuilder.setContentIntent(resultPendingIntent);

        //Issue the notification
        // Sets an ID for the notification for future management of the notification
        int mNotificationId = 2;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    public void createSimpleNotification(){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!");

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, ResultActivity.class);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                this,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        int notifyID = 1;
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(notifyID, mBuilder.build());
    }

    public void createService(){
        /*
         * Creates a new Intent to start the Service
         * IntentService. Passes a URI in the
         * Intent's "data" field.
         */
        Intent mServiceIntent = new Intent(this, ILeapesService.class);
        mServiceIntent.setData(Uri.parse("content://contacts/people/1"));
        //mServiceIntent.setData("data");
        mServiceIntent.putExtra("teste","joao");
        // Starts the IntentService
        startService(mServiceIntent);
//        Log.e("Running", String.valueOf(isMyServiceRunning(ILeapesService.class)));
//        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
//        alertDialog.setTitle("Service Status");
//        alertDialog.setMessage("Running: "+String.valueOf(isMyServiceRunning(ILeapesService.class)));
//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                new DialogInterface.OnClickListener(){
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//        alertDialog.show();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
