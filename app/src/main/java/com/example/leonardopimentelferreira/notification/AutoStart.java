package com.example.leonardopimentelferreira.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by leonardopimentelferreira on 4/21/15.
 *
 * This class is a Broadcast Receiver https://developer.android.com/reference/android/content/BroadcastReceiver.html
 */
public class AutoStart extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {
        /**
         * When the device is booted, this if statement is true and it is going to set an alarm that will be fired periodically.
         * AlarmReceiver will receive the event and will run the service to get new notifications
         */
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            //----------------------------------SET Alarm--------------------------------------
            try {
                Intent alarmReceiverIntent = new Intent(context, AlarmReceiver.class);
                // The pending intent that is triggered when the alarm fires.
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.ALARM_ID, alarmReceiverIntent, 0);
                // The app's AlarmManager, which provides access to the system alarm services.
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                // The alarm is going to be fired in 15 sec. and after that every 15 seconds (15000  milliseconds);
                alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 15000L, 15000L, pendingIntent);
                Log.e("AutoStart", "onReceive: AlarmManager update was set. ");
                // Cancel alarm
                // alarmManager.cancel(pendingIntent);
            } catch (Exception e) {
                Log.e("AutoStart", "onReceive: AlarmManager update was not set. " + e.toString());
            }
            //------------------------------------------------------------------------------------
        }
    }

}
